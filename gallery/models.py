import os

from django.db import models
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from feincms import settings
from feincms.templatetags import feincms_thumbnail


class Gallery(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Galleries'
        ordering = ('created',)

    def __unicode__(self):
        return self.title


class Photo(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    gallery = models.ForeignKey(Gallery, related_name='photos')
    photo = models.ImageField(
        max_length=255, upload_to=os.path.join(
            settings.FEINCMS_UPLOAD_PREFIX, 'gallery_photos'))
    caption = models.CharField(max_length=255, blank=True)

    class Meta:
        ordering = ('created',)

    def __unicode__(self):
        return self.caption or u'Image without caption'

    def get_thumbnail(self, type='cropscale', size=None):
        if not size:
            return self.photo.url

        thumbnailer = {
            'cropscale': feincms_thumbnail.CropscaleThumbnailer,
            }.get(type, feincms_thumbnail.Thumbnailer)
        return thumbnailer(self.photo, size)


class GalleryContent(models.Model):
    gallery = models.ForeignKey(Gallery)
    show_title = models.BooleanField(default=True)
    show_captions = models.BooleanField(default=True)

    class Meta:
        abstract = True
        verbose_name = 'Gallery'
        verbose_name_plural = 'Galleries'

    def render(self, **kwargs):
        return render_to_string(
            'gallery/render.html', {
                'gallery_content': self,
                'gallery': self.gallery,
                'show_title': self.show_title,
                'show_captions': self.show_captions,})

    @classmethod
    def initialize_type(cls, LARGE_FORMATS=None, THUMBNAIL_FORMATS=None):
        if LARGE_FORMATS:
            models.CharField(
                _('large format'),
                max_length=64,
                choices=LARGE_FORMATS,
                default=LARGE_FORMATS[0][0]
                ).contribute_to_class(cls, 'large_format')
        if THUMBNAIL_FORMATS:
            models.CharField(
                _('thumbnail format'),
                max_length=64,
                choices=THUMBNAIL_FORMATS,
                default=THUMBNAIL_FORMATS[0][0]
                ).contribute_to_class(cls, 'thumbnail_format')

    def get_small_version(self, photo):
        type, separator, size = getattr(
            self, 'thumbnail_format', '').partition(':')
        return photo.get_thumbnail(type, size)

    def get_large_version(self, photo):
        type, separator, size = getattr(
            self, 'large_format', '').partition(':')
        return photo.get_thumbnail(type, size)
