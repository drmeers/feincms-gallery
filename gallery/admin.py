from django.contrib import admin
from .models import Gallery, Photo

ADMIN_THUMBNAIL_SIZE = '100x100'
class PhotoAdmin(admin.ModelAdmin):
    list_display = ('admin_thumbnail', 'caption', 'gallery')
    list_filter = ('gallery',)
    def admin_thumbnail(self, photo):
        return '<img src="%s" />' % (
            photo.get_thumbnail(size=ADMIN_THUMBNAIL_SIZE).url)
    admin_thumbnail.allow_tags = True
    admin_thumbnail.short_description = 'Thumbnail'

class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 0

class GalleryAdmin(admin.ModelAdmin):
    inlines = [PhotoInline]

admin.site.register(Photo, PhotoAdmin)
admin.site.register(Gallery, GalleryAdmin)
