from django import template
register = template.Library()

@register.filter
def get_small_version(content, photo):
    return content.get_small_version(photo)

@register.filter
def get_large_version(content, photo):
    return content.get_large_version(photo)
